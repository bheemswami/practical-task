@extends('layouts.master')
@section('content')
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>User Details</h4>
          </div>
          <div class="form-parentBlock-inner"> 
            <table class="table table-bordered" cellspacing="0" width="100%">
              <tr>
                <td rowspan="3"><img class="service_icon" src="{{checkFile($user_data->profile_picture,'/uploads/user_img/')}}"/></td>
              </tr>
              <tr>
                <th>Name :</th>
                <td>{{$user_data->first_name}} {{$user_data->last_name}}</td>                
              </tr>
              <tr>               
                <th>Status :</th>
                <td>{!!($user_data->status==1) 
                  ? '<button class="btn btn-success btn-sm">Active</button>' 
                  : '<button class="btn btn-danger btn-sm">Inactive</button>'!!}
                </td>
              </tr>
              <tr>               
                <th>Email :</th>
                <td colspan="2">{{ $user_data->email }}</td>
              </tr>
              <tr>               
                <th>Phone Number :</th>
                <td colspan="2">{{ $user_data->phone }}</td>
              </tr>             
              <tr> 
                <td colspan="3" class="text-right">
                  <a class="btn btn-default btn-sm btn-icon icon-left" href="{{route('edit-user',[$user_data->id])}}" title="Edit"><i class="fa fa-pencil"></i>Edit</a>
                 </td>
              </tr>
            </table>
          </div>
     </div>

@stop
