@extends('layouts.master')
@section('content')

  @php 
      $flag=0;
      $heading='Add';
      if(isset($user_data) && !empty($user_data)){
          $flag=1;
          $heading='Update';
      }
  @endphp
      @if($flag==1)
          {{ Form::model($user_data,array('url'=>route('save-user'),'class'=>'add-user-form','files' => true))}}
          {{ Form::hidden('id',null)}}
      @else
          {{ Form::open(array('url'=>route('add-user'),'class'=>'add-user-form','files' => true))}}
      @endif
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>{{ $heading }} user</h4>
          </div>
         
          <div class="form-parentBlock-inner"> 
              
              {{ csrf_field() }}
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Firstname</label>
                {{Form::text('first_name',null,['class'=>"form-control",'placeholder'=>'Enter Fisrt Name'])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Lastname</label>
                {{Form::text('last_name',null,['class'=>"form-control",'placeholder'=>'Enter Last Name'])}}
              </div>
              
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Email</label>
                {{Form::text('email',null,['class'=>"form-control",'placeholder'=>'Enter Email'])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Phone Number</label>
                {{Form::text('phone',null,['class'=>"form-control",'placeholder'=>'Enter Phone Number'])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Password</label>
                {{Form::password('new_password',['class'=>"form-control",'placeholder'=>'Enter Password','id'=>'password'])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Confirm Password</label>
                {{Form::password('confirm_password',['class'=>"form-control",'placeholder'=>'Enter Password'])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Profile Picture</label>
                  {{Form::file('img',['class'=>""])}}
              </div>
             
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                 @if($flag==1)
                    <img class="service_icon" src="{{checkFile($user_data->profile_picture,'/uploads/user_img/')}}"/>
                  @endif
              </div>
          </div>
     </div>
     
        <div class="form-action">   
            <button type="submit" class="btn btn-primary submit-form">Submit</button>
          </div>
      {{Form::close()}}


@stop