@extends('layouts.master')
@section('content')
    <div class="row form-parentBlock">      
        <div class="block-title">
            <h4>Users</h4>
        </div>
        <div class="form-parentBlock-inner"> 
            <a href="{{route('add-user')}}" class="btn btn-success">Add User</a>
            <table id="tbl" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th> S.no. </th>
                        <th> Image </th>
                        <th>Fisrt Name</th>
                        <th>Last Name </th>
                        <th>Mobile</th>
                        <th>E-mail ID</th>                         
                        <th> Action </th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($users as $k=>$val)
                        <tr>
                            <td>{{$k+1}}. </td>
                            <td> <img class="img-circle dr_img" src="{{checkFile($val->profile_picture,'uploads/user_img/','user.png')}}"/> </td>
                            <td>{{$val->first_name}}</td>
                            <td>{{$val->last_name}}</td>
                            <td>{{$val->phone}}</td>
                            <td>{{$val->email}}</td> 
                            
                            <td>
                               <a class="btn btn-default btn-sm btn-icon icon-left" href="{{route('view-user',[$val->id])}}" title="View"><i class="fa fa-eye"></i>View</a>
                               <a class="btn btn-default btn-sm btn-icon icon-left" href="{{route('edit-user',[$val->id])}}" title="Edit"><i class="fa fa-pencil"></i>Edit</a>
                               <button class="btn btn-danger btn-sm btn-icon icon-left delete_btn" data-url="{{route('delete-user',[$val->id])}}" title="Delete"><i class="fa fa-times"></i>Delete</button>
                            </td>
                        </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
            {{$users->links()}}
        </div>
    </div>
@stop
