<!DOCTYPE html>
<html lang="en">
<head>
<title>Email</title>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- <link src="{{ asset('public/font/SF-UI-Display-Bold.ttf') }}"> -->
<style>

@import url('https://fonts.googleapis.com/css?family=Roboto:900');

@import url('https://fonts.googleapis.com/css?family=Roboto:700');

@import url('https://fonts.googleapis.com/css?family=Roboto:500');

@import url('https://fonts.googleapis.com/css?family=Roboto');

font-family{
  font-family: 'Roboto', sans-serif;
}
</style>
</head>
<body>


    <table style="max-width:600px;min-width:320px;border:1px solid #ededed" cellspacing="0" cellpadding="0" border="0" align="center">
  <tbody><tr>
    <td height="35"></td>
  </tr>
  <tr>
    <td><table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <td width="23"></td>
          <td>
            <img src="https://rightwayjob.com/assets/uploads/business_profile/default/logos/default-logo.png" style="height: 76px;"></td>
             </td>
          <td width="29"></td>
        </tr>
      </tbody></table></td>
  </tr>
  <tr>
    <td height="30"></td>
  </tr>
  <tr>
    <td><table style="background:url(https://ci3.googleusercontent.com/proxy/yFNFBNNT39DCSIXXvwoTgVNKGtPBF6sx5MWwKIV0n6bHJKeCdhh3IY63r_5yMPR6deu-yJw3fXI2DXo6oUnQZ91QHEvb6AC5xjVDd6rLrrjJYb2DJ6WJRDrK=s0-d-e1-ft#https://static.naukimg.com/ni/nistatic/niresman/images/img1_mail.gif) repeat-x #33779a;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#0f6892" background="https://ci3.googleusercontent.com/proxy/yFNFBNNT39DCSIXXvwoTgVNKGtPBF6sx5MWwKIV0n6bHJKeCdhh3IY63r_5yMPR6deu-yJw3fXI2DXo6oUnQZ91QHEvb6AC5xjVDd6rLrrjJYb2DJ6WJRDrK=s0-d-e1-ft#https://static.naukimg.com/ni/nistatic/niresman/images/img1_mail.gif">
        <tbody><tr>
          <td width="7" height="109" bgcolor="#FFFFFF"></td>
          <td width="36"></td>
          <td><font style="font-size:34px; font-family: 'Roboto', sans-serif; font-weight: 700;" color="#FFFFFF">{!! @$title !!}</font></td>
          <td width="158" valign="top"><img src="https://ci4.googleusercontent.com/proxy/22-UjsodutguGh1OElD7v9iIpo5lzkL-fjbVdpSya37qaY8qE94tFwk4QmuN6NxcAjyVRNbrmfTUvLqA6HjvAocqFTKg6ZNXc4J_UdR-oYHk2KXkcZvflo6m=s0-d-e1-ft#https://static.naukimg.com/ni/nistatic/niresman/images/img2_mail.gif" class="CToWUd" width="106" vspace="0" hspace="0" height="106" align="left"></td>
          <td width="7" bgcolor="#FFFFFF"></td>
        </tr>
      </tbody></table></td>
  </tr>
  <tr>
    <td><table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <td width="16" valign="top"><img src="https://ci5.googleusercontent.com/proxy/u4xucVS9MkP8Fky4Lc1_p4knRg4F3q--U1G2oUI_vezOxldNmt8k1ss-EJjmkTV_5gKZLzeaT2MdOrZce1Y_Qv_mNOItlRFpJuW6qEVxEK8FmcehBFrcPbkh=s0-d-e1-ft#https://static.naukimg.com/ni/nistatic/niresman/images/img3_mail.gif" class="CToWUd" width="9" vspace="0" hspace="0" height="8" align="right"></td>
          <td><table style="border:1px solid #d5d5d5;border-top:0px;font-family:Tahoma,Arial,sans-serif;font-size:14px;color:#474747;line-height:21px" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ededed">
              <tbody><tr>
                <td width="36" height="26"></td>
                <td></td>
                <td width="40"></td>
              </tr>
