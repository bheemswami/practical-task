@include('email_templates.header', ['title' => 'Verify your Account'])
            <tr>
                <td></td>
                <td width="492"><font style="font-size:16px; font-family: 'Roboto', sans-serif; font-weight: 700; "><strong>Dear {{$params['user_name']}},</strong></font><br>
                  <br>
                  <span style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight: 400;">
                 

                 Please confirm your email address by clicking on the button below. We'll communicate with you from time to time via this email so it's important that we have an up-to-date email address in our database.
                   </span> </td>
                <td></td>
              </tr>
              <tr>
                <td height="18"></td>
                <td></td>
                <td></td>
              </tr>
             
              <tr>
                <td height="5"></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td><table style="font-size:14px;color:#474747" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="left">
                    <tbody><tr>
                      <td colspan="5" height="12"></td>
                    </tr>
                    
                    <tr>
                      <td height="10"></td>
                      <td colspan="3"></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td height="30"></td>
                      <td colspan="3"><table cellspacing="0" cellpadding="0" border="0" align="left">
                          <tbody><tr>
                            <td style="font-family: 'Roboto', sans-serif; font-weight: 700;  font-size:15px; background:url(https://ci6.googleusercontent.com/proxy/hEkh7zBTx9yHq1F7yfESoWdrab4yGHqJDpde2BUoGyH5qy-e2vry42_f7rL8Jz3VBC7Mwp77YSk61A3GiXGuLHFV_CNcQizVSjrAcHPRgH7YhkHc0b59cE-j=s0-d-e1-ft#https://static.naukimg.com/ni/nistatic/niresman/images/butt_mail.gif) no-repeat" width="183" height="34" bgcolor="#f6d146" align="center">
                                <a href="{{route('email-verify',['uid'=>base64_encode($params['user_id']),'email'=>base64_encode($params['email'])])}}" style="text-decoration:none;font-size:15px;color:#484848;line-height:27px;height:34px;display:block" target="_blank">Verify Your E-mail</a>
                            </td>
                            <td width="47"></td>
                            <td width="220" align="right"></td>
                          </tr>
                        </tbody></table></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="5" height="12"></td>
                    </tr>
                  </tbody></table></td>
                <td></td>
              </tr>
            
              
              <tr>
                <td height="23"></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td height="32"></td>
                <td></td>
                <td></td>
              </tr>
@include('email_templates.footer')
@section('complement', 'Wish you all the best with your Job Search !')