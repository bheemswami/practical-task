<!DOCTYPE html>
<html>
<head>
	<title>Group of Education</title>
  <link rel="icon" href="{{url('public/images/favicon.png')}}" sizes="16x16" type="image/png">
  <link rel="stylesheet" href="{{URL::asset('public/plugins/fastselect/fastselect.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/admin_css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/admin_css/font-awesome.min.css')}}">
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/admin_css/admin_style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/admin_css/media.css')}}">
<script type="text/javascript" src="{{URL::asset('public/assets/jquery/jquery-3.2.1.min.js')}}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
<script type="text/javascript" src="{{URL::asset('public/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

        <script src="http://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="http://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>


</head>
<body class="vertical-layout 2-columns fixed-navbar pace-done vertical-menu menu-expanded">

<!-- Header Fixed Start -->
<nav class="header-navbar navbar navbar-fixed-top">
  <div class="navbar-wrapper">
    <div class="navbar-header">
      <a href="{{url('public//admin/dashboard')}}" class="navbar-brand">
        <img alt="stack admin logo" src="{{url('public/images/logo12.png')}}" width="180px" class="brand-logo">
      </a>
      <a class="menu-srink is-active"><i class="fa fa-bars"></i></a>
    </div>
    
    <div class="navbar-container content container-fluid">
      <ul class="nav navbar-nav pull-right">
       {{--  <li class="dropdown dropdown-notification nav-item">
          <a href="#" data-toggle="dropdown" class="nav-link nav-link-label">
            <i class="fa fa-bell"></i>
            <span class="tag tag-pill tag-up">2</span>
          </a>
          <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
            <li class="dropdown-menu-header">
              <h6 class="dropdown-header m-0">
                <span class="">Notifications</span>
              </h6>
            </li>
            <li class="list-group scrollable-container">
              <a href="" class="list-group-item">
                <div class="media">
                  <div class="media-left valign-middle">
                    <i class="fa fa-envelope icon-bg-circle"></i>
                  </div>
                  <div class="media-body">
                    <h6 class="media-heading">You have new Booking!</h6>
                    <p class="notification-text">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                  </div>
                </div>
              </a>
              <a href="" class="list-group-item">
                <div class="media">
                  <div class="media-left valign-middle">
                    <i class="fa fa-envelope icon-bg-circle"></i>
                  </div>
                  <div class="media-body">
                    <h6 class="media-heading">You have new Booking!</h6>
                    <p class="notification-text">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="dropdown-menu-footer">
              <a href="" class="dropdown-item">Read all notifications</a>
            </li>
          </ul>
        </li> --}}
        
        <!-- <li class="dropdown dropdown-notification nav-item">
          <a href="#" data-toggle="dropdown" class="nav-link nav-link-label">
            <i class="fa fa-envelope"></i>
            <span class="tag tag-pill tag-up">3</span>
          </a>
          <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
            <li class="dropdown-menu-header">
              <h6 class="dropdown-header m-0">
                <span class="grey darken-2">Messages</span>                
              </h6>
            </li>
            <li class="list-group scrollable-container" >
              <a href="" class="list-group-item">
                <div class="media">
                  <div class="media-left">
                    <span class="avatar avatar-sm avatar-busy rounded-circle">
                      <img src="{{url('public/images/avatar-s-1.png')}}"  alt="avatar">
                    </span>
                  </div>
                  <div class="media-body">
                    <h6 class="media-heading">Bret Lezama</h6>
                    <p class="notification-text">I have seen your work, there is</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="dropdown-menu-footer">
              <a href="" class="dropdown-item">Read all messages</a>
            </li>
          </ul>
        </li> -->
        
        <li class="dropdown dropdown-user nav-item">
          <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
            <span class="avatar avatar-online">
              <img src="{{url('public/images/default/user_img.png')}}" alt="avatar">
            </span>     
            <span class="user-name">{{ucfirst(Auth()->user()->first_name)}}</span>
            <i class="fa fa-angle-down"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            {{-- <a href="{{route('profile')}}" class="dropdown-item"><i class="ft-user"></i> Edit Profile</a> --}}
            <!-- <a href="#" class="dropdown-item"><i class="ft-mail"></i> My Inbox</a>
            <a href="#" class="dropdown-item"><i class="ft-check-square"></i> Task</a>
            <a href="#" class="dropdown-item"><i class="ft-comment-square"></i> Chats</a>
            <div class="dropdown-divider"></div> -->
            <a href="{{route('log-me-out')}}" class="dropdown-item"><i class="ft-power"></i> Logout</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- Header Fixed End -->

<!-- sidebar menu Start -->
@include('layouts.sidebar')
<!-- sidebar menu End -->
<!-- -->
<div class="app-content content">
  <div class="content-wrapper">
    @if(session('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            {{session('success')}}
        </div>
    @elseif(session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            {{session('error')}}
        </div>
    @elseif(session('info'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            {{session('info')}}
        </div>
    @endif
    @yield('content')
  </div>
</div>
<!-- <script type="text/javascript" src="js/jquery.nicescroll.js"></script> -->
<script type="text/javascript" src="{{URL::asset('public/js/jquery.validate.min.js')}}"></script>


<script type="text/javascript" src="{{URL::asset('public/plugins/sweetalert2-7.0.0/sweetalert2.all.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{URL::asset('public/plugins/sweetalert2-7.0.0/sweetalert2.min.css')}}">

<link href="{{URL::asset('public/plugins/summernote-0.8.8/dist/summernote.css')}}" rel="stylesheet">
<script src="{{URL::asset('public/plugins/summernote-0.8.8/dist/summernote.js')}}"></script>



<script type="text/javascript" src="{{URL::asset('public/js/admin.js')}}"></script>
<script>
   
  if(jQuery(window).width()>="767"){
  // Menu Collapsed and expanded
  jQuery('.menu-srink').click(function(){
    if($(this).hasClass('is-active')){
      $(this).removeClass('is-active').addClass('is-deactive');
      $('body').addClass('menu-collapsed').removeClass('menu-expanded');
    }
    else{
      $(this).removeClass('is-deactive').addClass('is-active');      
      $('body').addClass('menu-expanded').removeClass('menu-collapsed');
    } 
  });
  }else{
    jQuery('.menu-srink').click(function(){
      jQuery('.main-menu').fadeToggle('slow'),200;
    });
  }
  // Toggle Sub Menu
  jQuery(document).ready(function(){
    jQuery('.root-level.has-sub').click(function(){
      jQuery('.root-level.has-sub').removeClass('opened');
      jQuery('.root-level.has-sub ul').removeClass('visible');
      jQuery(this).children('ul').toggleClass('visible');
      jQuery(this).toggleClass('opened');
    });
  });
  $(function () {
    $('.cal_date_time').datetimepicker();
});
</script>
<script>

     $('#select_all_checkbox').on('click',function(){
            if(this.checked){
                $('.data_checkbox').each(function(){
                    this.checked = true;
                });
            }else{
                 $('.data_checkbox').each(function(){
                    this.checked = false;
                });
            }
    });
    
    $('.data_checkbox').on('click',function(){
        if($('.data_checkbox:checked').length == $('.data_checkbox').length){
            $('#select_all_checkbox').prop('checked',true);
        }else{
            $('#select_all_checkbox').prop('checked',false);
        }
    });

    </script>
</body>
</html>
