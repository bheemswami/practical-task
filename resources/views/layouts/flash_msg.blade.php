@if(session('success'))
    <div class="alert alert-success alert-dismissible bg_bdr_none" role="alert">
        <button type="button" class="close bg_bdr_none" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {!! session('success') !!}
    </div>
@elseif(session('error'))
    <div class="alert alert-danger alert-dismissible bg_bdr_none" role="alert">
        <button type="button" class="close bg_bdr_none" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {!! session('error') !!}
    </div>
@elseif(session('info'))
    <div class="alert alert-warning alert-dismissible bg_bdr_none" role="alert">
        <button type="button" class="close bg_bdr_none" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {!! session('info') !!}
    </div>
@endif
