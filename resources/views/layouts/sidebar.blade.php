<div class="main-menu menu-fixed nano">
  <ul id="main-menu" class="nano-content" style="">
  
   
    
    <li class="root-level has-sub">
      <a><i class="fa fa-male"></i>Users</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('users')}}"><i class="fa fa-arrow-right"></i>All Users</a></li>     
      </ul>
    </li>
    
  </ul>
</div>

<script>
   $(document).ready(function() {
        var permalink_nodomain = window.location.href;
        console.log(permalink_nodomain);
        $('.root-level a[href="' + permalink_nodomain + '"]').parents('li').addClass('active').addClass('opened');
        $('.root-level a[href="' + permalink_nodomain + '"]').parents('.sub_menu_ul').addClass('visible');
        $('.root-level a[href="' + permalink_nodomain + '"]').parent('.sub_menu_ul li').addClass('active_child');
    });
  </script>