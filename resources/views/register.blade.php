<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
	<link rel="icon" href="{{url('public/images/favicon.png')}}" sizes="16x16" type="image/png">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/admin_css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/admin_css/font-awesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/admin_css/admin_style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/admin_css/media.css')}}">
  <script type="text/javascript" src="{{URL::asset('public/assets/jquery/jquery-3.2.1.min.js')}}"></script>
</head>
<body style="background: #fff;">
@include('layouts.flash_msg')
<div id="login-panel">
  <div id="login-panel-inner">
    <div class="login-panel-header text-center">      
        <img class="img-responsive center-block" src="{{url('public/images/logo12.png')}}">
        
    </div>
    <!-- <div class="container"> -->
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-pannel">
				{{ Form::open(array('url'=>route('register-process'),'class'=>'reg-form','id' =>'reg-form','files' => true))}}
                {{ csrf_field() }}
            <div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                <label for="">Firstname</label>
                {{Form::text('first_name',null,['class'=>"form-control",'placeholder'=>'Enter Fisrt Name'])}}
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                <label for="">Lastname</label>
                {{Form::text('last_name',null,['class'=>"form-control",'placeholder'=>'Enter Last Name'])}}
              </div>
              
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                <label for="">Email</label>
                {{Form::text('email',null,['class'=>"form-control",'placeholder'=>'Enter Email'])}}
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                <label for="">Phone Number</label>
                {{Form::text('phone',null,['class'=>"form-control",'placeholder'=>'Enter Phone Number'])}}
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                <label for="">Password</label>
                {{Form::password('password',['class'=>"form-control",'placeholder'=>'Enter Password','id'=>'password'])}}
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                <label for="">Confirm Password</label>
                {{Form::password('confirm_password',['class'=>"form-control",'placeholder'=>'Enter Password'])}}
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                <label for="">Profile Picture</label>
                  {{Form::file('img',['class'=>"prof-pic",'id'=>'file-1a'])}}
              </div>
             
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                <button type="submit" class="btn btn-info">Register</button>
                <a href="{{route('login')}}" class="btn btn-success">Login</a>
              </div>
                	</div>
					
				{{ Form::close() }}
			</div>

		<!-- </div> -->
	</div>
</div>
<script type="text/javascript" src="{{URL::asset('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/js/frontpanel.js')}}"></script>
</body>
</html>