 $(document).ready(function() {
        $('.txt-editor').summernote({
        	height: 160,
        });
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD h:mm'
        });
        $('#example,#datatbl,#dataTables-example,.data_table').DataTable();
 });

if($(".add-user-form")[0] != undefined){ 
    $(".add-user-form").validate({
        rules: {
            email: {required: true},
            first_name: {required: true},
            last_name: {required: true},
            phone: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            password: {
                //required: true,
                minlength: 6,
            },
            confirm_password: {
                //required: true,
                minlength: 6,
                equalTo: "#password"
            },
          
        },
        messages: {
            email: {
                required: "Please enter your email address.",
                email: "Please enter a valid email address.",
            },
           
        },
    });
}
//alerts
$('.delete_btn').click(function(){
	var deleteUrl = $(this).data('url');
	swal({
	  title: 'Are you sure?',
	  text: "You won't be able to revert this!",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, delete it!'
	}).then(function (result) {
	  if (result.value) {
	    window.location.href=deleteUrl;
	  }
	});
});
