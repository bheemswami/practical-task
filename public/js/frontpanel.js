
function previewImage(input,elem,defaultImg='') {
    if (input.files && input.files[0] && $.inArray(input.files[0].type, ['image/jpeg','image/jpg','image/png']) != -1) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(elem).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    } else {
        $(elem).attr('src',defaultImg);
    }
}
function printSelectedFileName(fieldVal,elem) {
    var fieldVal = fieldVal.replace("C:\\fakepath\\", "");
    if (fieldVal != undefined || fieldVal != "") {
      $(elem).text(fieldVal);
    }
}


// **************************** start validation js ****************************

if($(".reg-form")[0] != undefined){ 
    $(".reg-form").validate({
        rules: {
            email: {required: true},
            first_name: {required: true},
            last_name: {required: true},
            phone: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            password: {
                required: true,
                minlength: 6,
            },
            confirm_password: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            img: {
                required: true,
                accept: "image/jpg,image/jpeg,image/png",                
            }
        },
        messages: {
            email: {
                required: "Please enter your email address.",
                email: "Please enter a valid email address.",
            },
            img: {
                accept: 'Please upload a image with valid extension (.jpg, .jpeg and .png)',
            },
        },
    });
}

if($(".login-form")[0] != undefined){    
    $(".login-form").validate({
        rules: {
            email: {required: true},
            password: {required: true,minlength: 6,},
        },
    });
}

// ************************************************** End validation js *********************************************