<?php 
use App\User;
use App\Registration;
use App\PageBanner;
use App\Setting;
use App\Service;
use App\Faq;
function loggedUserProfile(){
        return User::where('id',Auth::user()->id)->first();
}
function getSiteSetting(){
    return Setting::first();
}
function numberRange($low,$high,$step){
    $num = range($low,$high,$step);
    foreach($num as $v){
        $keyVals[$v]=$v;
    }
    return $keyVals;
}
function getDigitRange($s,$e,$label=null){
    $range=[];
    for($i=$s;$i<=$e;$i++){
        $range[$i]=$i;
    }
    if($label=='year'){
        $range[$s].=' year';
        $range[end($range)].=' + year';
    }
    return $range;
}
function genRandomValue($length=5,$type='digit',$prefix=null){
    if($type=='digit'){
        $characters = date('Ymd').'123456789987654321564738291918273645953435764423'.time();
    } else {
        $characters = date('Ymd').'123456789TRANSACTIONID987654321ABCDEFGHIJKLMNOPQRSTUVWXYZ111BHEEMSWAMI9OO14568O8BIKANER1RAJASTHAN334OO1'.time();
    }
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $prefix.$randomString;
}

function encryptVal($val){
    return str_rot13($val);
}
function decryptVal($val){
    return str_rot13($val);
}
function dateConvert($date=null,$format=null){
    if($date==null)
        return date($format);
    if($format==null)
        return date('Y-m-d',strtotime($date));
    else 
        return date($format,strtotime($date));
}

function timeConvert($time,$format=null){
	if($format==null)
		return date('H:i:s',strtotime($time));
	else 
		return date($format,strtotime($time));
}
function timeFormatAmPm($time=null){
    if($time==null || $time==''){
        return '';
    }
    $exp = explode(' ', $time);
    $temp = date_parse($exp[0]);
    $temp['minute'] = str_pad($temp['minute'], 2, '0', STR_PAD_LEFT);
    return date('h:i a', strtotime($temp['hour'] . ':' . $temp['minute']));
}
 function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

function limit_text($text, $limit) {
  if (strlen($text) > $limit) {
        $text = substr($text, 0, $limit) . '...';
  }
  return $text;
}
function limit_words($string, $word_limit)
{
    if (str_word_count($string, 0) > $word_limit) {
        $words = explode(" ",$string);
        return implode(" ",array_splice($words,0,$word_limit)).'...';
    }
    return $string;
}

function checkFile($filename,$path,$default=null) {
    $src=url('public/images/default/'.$default);
    $path='public/'.$path;
    if($filename != NULL && $filename !='' && $filename != '0')
    {
        $file_path = app()->basePath($path.$filename);
        if(File::exists($file_path)){
            $src=url($path.$filename);
        } 
    }
    return $src;      
}
function unlinkImg($img,$path) {
    if($img !=null || $img !='')
    {
        $path='public/'.$path;
        $image_path = app()->basePath($path.$img);
        if(File::exists($image_path)) 
            unlink($image_path);
    }       
}

function getLatLong($address){
        $prepAddr = str_replace(' ','+',$address);
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
        $output= json_decode($geocode);
        if(!empty($output->results))
            return ['lat'=>$output->results[0]->geometry->location->lat,'long'=>$output->results[0]->geometry->location->lng];
        return ['lat'=>'','long'=>''];
}

function emailExistUsers($email=null,$ex_id=null){
    if($email!=null){
        if($ex_id!=null)
            return User::where('id','!=',$ex_id)->where('email',$email)->get()->count();
        else
            return User::where('email',$email)->get()->count();
    }
    return 0;
}
function mobNumExistUsers($mobile=null,$ex_id=null){
    if($mobile!=null){
        if($ex_id!=null)
            return User::where('id','!=',$ex_id)->where('mobile',$mobile)->get()->count();
        else
            return User::where('mobile',$mobile)->get()->count();
    }
    return 0;
}
function getSettings(){
    Cache::remember('settingData', config('constants.CACHING_TIME'), function() {
        return Setting::first();
    });
    return Cache::get('settingData');
}

// use when new user register
function getNextRegNo($request){

    $regNo = date('Yd').genRandomValue(4);
    if(User::where('reg_number',$regNo)->count()>0){
        $regNo = date('Yd').genRandomValue(4);
        //return 0;
    }
    return $regNo;
}

// use when user register for test series
function getNextRegNo2($request){

    $regData = Registration::orderBy('reg_no','DESC')->first();
    if($regData){
        $regNoNext = $regData->reg_no+1;
    } else {
        $regNoNext ='5001';
    }
    return $regNoNext;
}

function sendPushAndroidNotification($registatoin_ids,$data) {
    $authKey = 'AAAAx1g755Y:APA91bFDji3mq-g0jLTJ7J89W73Vuntl3GppUsBOSFyjzJYKY3oUJSJ2czMknHTiYQVD_PKzUiuvQplSsxoirRwlB9BOBVyVH7mCE1ys7iypT0RjLWa6PnDPapSfH5Oj_mGEynbfS-6W';
    $url = 'https://fcm.googleapis.com/fcm/send';    
    $fields = array(
        'registration_ids' => $registatoin_ids,
        'notification' => array( 'title' => $data['title'], 'body' => $data['body'],'sound'=>true),
        'data' => $data
    );
    $fields = json_encode ( $fields );
    $headers = array ('Authorization: key=' . $authKey, 'Content-Type: application/json');

    $ch = curl_init ();
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_POST, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

    $result = curl_exec ( $ch );
    curl_close ( $ch );
    dd($result);
    return $result;
    
}

