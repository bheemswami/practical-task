<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Hash,DB,Session,Auth;
use App\User;

class UserController extends Controller
{
    private $core;
    public $data=[];
    public function __construct()
    {
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    

/* *************** * Start User Functions * *************** */
    public function index()
    {
        $this->data['users']=User::where('status',1)->paginate(10);
        return view('users.users_list', $this->data);
    }
   
    public function addUser(Request $request)
    {
        if($this->core->checkUserExist($request->email,$request->id)){
            return redirect()->back()->with(['error' => config('constants.FLASH_EMAIL_ADDRESS_EXIST').' ('.$request->email.')']);
        }
        if($request->hasFile('img')){
            $filename=$this->core->fileUpload($request->file('img'),'uploads/user_img');
            $request->merge(['profile_picture'=>$filename]);
        }
        $request->merge(['password'=>Hash::make($request->new_password),'added_by'=>Auth::id()]);
        $user = User::create($request->except(['_token','img','new_password','confirm_password']));
        if($user){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_ADD_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_ADD_0')]);
    }

     public function editUser(Request $request)
    {
         $this->data['user_data']=User::whereId($request->id)->first();
        return view('users.add_edit_user',$this->data);
    }

    public function saveUser(Request $request)
    {
        if($this->core->checkUserExist($request->email,$request->id)){
            return redirect()->back()->with(['error' => config('constants.FLASH_EMAIL_ADDRESS_EXIST').' ('.$request->email.')']);
        }
        $userData = User::find($request->id);
        if($request->new_password!=''){
            $request->merge(['password'=>Hash::make($request->new_password)]);
        }
        if($request->hasFile('img')){
            $filename=$this->core->fileUpload($request->file('img'),'uploads/user_img');
            $this->core->unlinkImg($userData->profile_picture,'uploads/user_img');
            $request->merge(['profile_picture'=>$filename]);
        }

        $user = $userData->update($request->except(['_token','img','new_password','confirm_password']));
        if($user){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_1')]);
        }
        return redirect()->route('manage-users')->with(['error' => config('constants.FLASH_REC_UPDATE_0')]);
    }
    public function viewUser(Request $request){
        $this->data['user_data']=User::whereId($request->id)->first();
        return view('users.user_details',$this->data);
    }
    public function deleteUser(Request $request)
    {
        if(User::whereId($request->id)->delete()){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }

    public function logMeOut(){
       Auth::logout();
        return redirect()->route('login')->with(['error' => config('constants.FLASH_SUCCESS_LOGOUT')]);
    }
/* *************** * End User Functions * *************** */


}
