<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth,Hash,Session;

use App\User;

class HomeController extends Controller
{
    private $core;
    public $data=[];
    public function __construct()
    {
        //$this->middleware('auth');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function index()
    {
        return view('home');
    }

    public function registerProcess(Request $request)
    {
       if($this->core->checkUserExist($request->email)){
            return redirect()->back()->with(['error' => config('constants.FLASH_EMAIL_ADDRESS_EXIST').' ('.$request->email.')']);
        }
        if($request->hasFile('img')){
            $filename=$this->core->fileUpload($request->file('img'),'uploads/user_img');
            $request->merge(['profile_picture'=>$filename]);
        }
        $request->merge(['password'=>Hash::make($request->password)]);
        $user = User::create($request->except(['_token','img','confirm_password']));
        if($user){
            $this->sendVerificationMail($user);
            return redirect()->route('login')->with(['success' => config('constants.FLASH_MSG_SEND_VERIFY_EMAI')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REG_0')]);
    }

    public function LoginProcess(Request $request)
    {

        $email = $request->email;
        $password = $request->password;
        $res=User::where('email',$email)->first();
        if($res){
            if($res->email_verified==0){
                Session::put('tmp_email',$email);
                $url='<a href="'.route('send-verify-mail').'">Click here</a>';
                $msg= str_replace(['##URL##'],[$url],config('constants.FLASH_VERIFICATION_EMAIL'));
                return redirect()->back()->with(['info' => $msg]);
            }
        }
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            return redirect()->route('users')->with(['success' => config('constants.FLASH_SUCCESS_LOGIN')]);
        } else {
            return redirect()->back()->with(['success' => config('constants.FLASH_INVALID_CREDENTIAL')]);
        }
    }

    public function sendVerificationMail($user=null){         
        if($user==null && Session::has('tmp_email')){
            $userRow=User::where('email_verified',0);
            $userRow->where('email',Session::get('tmp_email'));
            $user = $userRow->first();
        }
        if($user){
            $mailData['params']=['email'=>$user->email,'user_id'=>$user->id,'user_name'=>$user->first_name,'subject'=>'Please verify your account','template'=>'verify_email'];
            $this->core->SendEmail($mailData);
            return redirect()->back()->with(['success' => 'We have sent a confirmation E-mail to '.$user->email.'. Please verify account.']);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_NOT_ALLOW_URL')]);
    }
    public function emailVerify(Request $request)
    {
        $user = User::where('id', base64_decode($request->uid))->where('email', base64_decode($request->email))->first();
        if ($user) {
            $user->email_verified = 1;
            $user->save(); 
            if (Auth::loginUsingId($user->id)) {
                return redirect()->route('users')->with(['success' => config('constants.FLASH_EMAIL_VERIFY_1')]);
            } else {
                return redirect()->route('login')->with(['success' => config('constants.FLASH_INVALID_CREDENTIAL')]);
            }          
        } 
        return redirect('/');
    }
}
