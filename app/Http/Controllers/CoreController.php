<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PHPMailerAutoload; 
use PHPMailer\PHPMailer\PHPMailer;
use DB,File,Auth;
use App\User;
class CoreController extends Controller
{   
    function fileUpload($file,$path,$original_name=0)
    {
        $filename='';
        $full_path='public/'.$path;
        File::isDirectory($full_path) or File::makeDirectory($full_path, 0777, true, true);
        File::isDirectory($full_path.'/thumbnail') or File::makeDirectory($full_path.'/thumbnail', 0777, true, true);
        if($file!=''){
            if($original_name==1){
                $filename=$file->getClientOriginalName();                
            } else {
                $filename=md5($file->getClientOriginalName()).'_'.time().'.'.$file->getClientOriginalExtension();    
            }
            $file->move($full_path, $filename); 
        }
       
        return $filename;
   
    }

   
    function unlinkImg($img,$path) {
        if($img !=null || $img !='')
        {
            $path='public/'.$path.'/';
            $image_path = app()->basePath($path.$img);
            if(File::exists($image_path)) 
                unlink($image_path);
        }       
    }
   
    public function SendEmail($data){
        $flag=0;
        $res = FALSE; 
        $mail = $this->mailConfig();
        
        $mail->addAddress($data['params']['email']);
        $mail->Subject = $data['params']['subject'];
        if(isset($data['params']['template']) && $data['params']['template']!=''){
            $mail->Body = view('email_templates/'.$data['params']['template'],$data);
        } else {
            $mail->Body = $data['params']['msg'];
        }
        $mail->isHTML(true);
        if($mail->Send()) {            
            $res = TRUE;
        }
        return $res;
     }

    function mailConfig(){
        $mail = new PHPMailer;
        $mail->isSMTP();
       // $mail->SMTPDebug  = 1; 
        $mail->SMTPAuth = true;
        $mail->Host = config('constants.email_config')['host'];
        $mail->Username = config('constants.email_config')['username'];
        $mail->Password = config('constants.email_config')['password'];
        $mail->SMTPSecure = config('constants.email_config')['smtp_secure'];
        $mail->Port = config('constants.email_config')['smtp_port'];
        $mail->From = config('constants.email_config')['from_email'];
        $mail->FromName = config('constants.email_config')['from_name'];
        $mail->addReplyTo(config('constants.email_config')['reply_to'], 'Email Notification');  
        return $mail;
    }

    function checkUserExist($email=null,$except_id=null){
        if($email!=null){
            if($except_id!=null)
                return User::where('id','!=',$except_id)->where('email',$email)->get()->count();
            else
                return User::where('email',$email)->get()->count();
        }
        return 0;

     }

}