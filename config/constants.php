<?php
return [
 
    'email_config'=>[
        'from_email'    =>env('MAIL_FROM', 'bheemswami808@gmail.com'),
        'from_name'     =>env('MAIL_FROM_NAME', 'Bheem'),
        'reply_to'      =>env('MAIL_REPLY_TO', 'bheemswami808@gmail.com'),
        'host'          =>env('MAIL_HOST', ''),
        'username'      =>env('MAIL_USERNAME', ''),
        'password'      =>env('MAIL_PASSWORD', ''),
        'smtp_secure'   =>env('MAIL_SECURE', ''),
        'smtp_port'     => env('MAIL_PORT', 587),
    ],
   
    'genders'=>[
      'Mr.'=>'Male',
      'Mrs.'=>'Female.',
      'Miss.'=>'Female.'
    ],
    
    'status'=>[
        0=>'Inactive',
        1=>'Active',
    ],
   
    'FLASH_SUCCESS_LOGIN'=>'Successfully loggedin.',
    'FLASH_SUCCESS_LOGOUT'=>'You have successfully logged out',
    'FLASH_INVALID_CREDENTIAL'=>'Username/Password does not match.',
    'FLASH_INVALID_EMAIL_ADDRESS'=>'Please enter valid email address.',
    'FLASH_EMAIL_VERIFY_1'=>'Your email address has been verified successfully. ',
    'FLASH_EMAIL_VERIFY_0'=>'Your email address not verified.Verified before loggin. ',
    'FLASH_EMAIL_ADDRESS_EXIST'=>'This email id is already registered.',  
    'FLASH_ERROR_PASSWORD_UNMATCH'=>'Current password not match. Try again!',

    'FLASH_REG_1'=>'Record has been added successfully.',
     'FLASH_REG_0'=>'Please check your internet connection and try again.',
 
    'FLASH_REC_ADD_1'=>'Record has been added successfully.',
    'FLASH_REC_ADD_0'=>'Please check your internet connection and try again.',
    'FLASH_REC_UPDATE_1'=>'Record has been updated successfully.',
    'FLASH_REC_UPDATE_0'=>'Record not updated.Try again!',
    'FLASH_REC_DELETE_1'=>'Record has been deleted successfully.',
    'FLASH_REC_DELETE_0'=>'Record not deleted.Try again!',

   'FLASH_MSG_SEND_VERIFY_EMAI'=>'Verification email send successfully your mailbox. If the email takes more than 15 minutes to appear in your mailbox, please check your spam folder.',
    'FLASH_VERIFICATION_EMAIL'=>'Please verify your email to login. ##URL## to resend the verification email.',
];


