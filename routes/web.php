<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();
Route::get('login', function(){ return view('login');})->name('login');
Route::get('register', function(){ return view('register');})->name('register');
Route::get('add-user', function(){ return view('users.add_edit_user');})->name('add-user');

Route::post('login-process', 'HomeController@LoginProcess')->name('login-process');
Route::post('register-process', 'HomeController@registerProcess')->name('register-process');
Route::get('email-verify', 'HomeController@emailVerify')->name('email-verify');
Route::get('send-verify-mail', 'HomeController@sendVerificationMail')->name('send-verify-mail');

Route::group(['middleware' => ['auth']], function()
{
	Route::get('users', 'UserController@index')->name('users');
	Route::get('view-user/{id}', 'UserController@viewUser')->name('view-user');
	Route::post('add-user', 'UserController@addUser')->name('add-user');
	Route::post('save-user', 'UserController@saveUser')->name('save-user');
	Route::get('edit-user/{id}', 'UserController@editUser')->name('edit-user');
	Route::get('delete-user/{id}', 'UserController@deleteUser')->name('delete-user');
	Route::get('log-out', 'UserController@logMeOut')->name('log-me-out');
});
